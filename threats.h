int n_threats = 35;

threat_t possible_threats[3][35] = {
    {
        {
            {-1, -1, -1, -1, 0},
            {false, false, false, false, true},
            5,
            -500000,
        },
        {
            {0, -1, -1, -1, -1},
            {true, false, false, false, false},
            5,
            -500000,
        },
        {
            {0, 1, 1, 1, 1},
            {true, false, false, false, false},
            5,
            500000,
        },
        {
            {1, 0, 1, 1, 1},
            {false, true, false, false, false},
            5,
            500000,
        },
        {
            {1, 1, 0, 1, 1},
            {false, false, true, false, false},
            5,
            500000,
        },
        {
            {1, 1, 1, 0, 1},
            {false, false, false, true, false},
            5,
            500000,
        },
        {
            {1, 1, 1, 1, 0},
            {false, false, false, false, true},
            5,
            500000,
        },
        {
            {0, 0, 1, 1, 1, 0},
            {false, true, false, false, false, true},
            6,
            50000,
        },
        {
            {0, 1, 1, 1, 0, 0},
            {true, false, false, false, true, false},
            6,
            50000,
        },
        {
            {0, 1, 1, 0, 1, 0},
            {true, false, false, true, false, true},
            6,
            50000,
        },
        {
            {0, 1, 0, 1, 1, 0},
            {true, false, true, false, false, true},
            6,
            50000,
        },
        {
            {0, 0, -1, -1, -1, 0},
            {false, true, false, false, false, true},
            6,
            -50000,
        },
        {
            {0, -1, -1, -1, 0, 0},
            {true, false, false, false, true, false},
            6,
            -50000,
        },
        {
            {0, -1, -1, 0, -1, 0},
            {true, false, false, true, false, true},
            6,
            -50000,
        },
        {
            {0, -1, 0, -1, -1, 0},
            {true, false, true, false, false, true},
            6,
            -50000,
        },
        {
            {0, 0, 1, 1, 1, 0},
            {false, true, false, false, false, true},
            6,
            50000,
        },
        {
            {0, 1, 1, 1, 0, 0},
            {true, false, false, false, true, false},
            6,
            50000,
        },
        {
            {0, 1, 1, 0, 1, 0},
            {true, false, false, true, false, true},
            6,
            50000,
        },
        {
            {0, 1, 0, 1, 1, 0},
            {true, false, true, false, false, true},
            6,
            50000,
        },
        {
            {0, -1, -1, 0},
            {true, false, false, true},
            4,
            -100,
        },
        {
            {-1, 0, -1, 0},
            {false, true, false, true},
            4,
            -100,
        },
        {
            {0, -1, 0, -1},
            {true, false, true, false},
            4,
            -100,
        },
        {
            {0, -1, 0},
            {true, false, true},
            3,
            -40,
        },
        {
            {0, -1, -1},
            {true, false, false},
            3,
            -40,
        },
        {
            {-1, -1, 0},
            {false, false, true},
            3,
            -40,
        },
        {
            {0, 1, 1, 0},
            {true, false, false, true},
            4,
            100,
        },
        {
            {1, 0, 1, 0},
            {false, true, false, true},
            4,
            100,
        },
        {
            {0, 1, 0, 1},
            {true, false, true, false},
            4,
            100,
        },
        {
            {0, 1, 0},
            {true, false, true},
            3,
            100,
        },
        {
            {0, 1, 1},
            {true, false, false},
            3,
            100,
        },
        {
            {1, 1, 0},
            {false, false, true},
            3,
            100,
        },
        {
            {-1, 0},
            {false, true},
            2,
            -1,
        },
        {
            {0, -1},
            {true, false},
            2,
            -1,
        },
        {
            {1, 0},
            {false, true},
            2,
            -1,
        },
        {
            {0, 1},
            {true, false},
            2,
            -1,
        },
    },
    {
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
    },
    {
        {
            {1, 1, 1, 1, 0},
            {false, false, false, false, true},
            5,
            500000,
        },
        {
            {0, 1, 1, 1, 1},
            {true, false, false, false, false},
            5,
            500000,
        },
        {
            {0, -1, -1, -1, -1},
            {true, false, false, false, false},
            5,
            -500000,
        },
        {
            {-1, 0, -1, -1, -1},
            {false, true, false, false, false},
            5,
            -500000,
        },
        {
            {-1, -1, 0, -1, -1},
            {false, false, true, false, false},
            5,
            -500000,
        },
        {
            {-1, -1, -1, 0, -1},
            {false, false, false, true, false},
            5,
            -500000,
        },
        {
            {-1, -1, -1, -1, 0},
            {false, false, false, false, true},
            5,
            -500000,
        },
        {
            {0, 0, -1, -1, -1, 0},
            {false, true, false, false, false, true},
            6,
            -50000,
        },
        {
            {0, -1, -1, -1, 0, 0},
            {true, false, false, false, true, false},
            6,
            -50000,
        },
        {
            {0, -1, -1, 0, -1, 0},
            {true, false, false, true, false, true},
            6,
            -50000,
        },
        {
            {0, -1, 0, -1, -1, 0},
            {true, false, true, false, false, true},
            6,
            -50000,
        },
        {
            {0, 0, 1, 1, 1, 0},
            {false, true, false, false, false, true},
            6,
            50000,
        },
        {
            {0, 1, 1, 1, 0, 0},
            {true, false, false, false, true, false},
            6,
            50000,
        },
        {
            {0, 1, 1, 0, 1, 0},
            {true, false, false, true, false, true},
            6,
            50000,
        },
        {
            {0, 1, 0, 1, 1, 0},
            {true, false, true, false, false, true},
            6,
            50000,
        },
        {
            {0, 0, -1, -1, -1, 0},
            {false, true, false, false, false, true},
            6,
            -50000,
        },
        {
            {0, -1, -1, -1, 0, 0},
            {true, false, false, false, true, false},
            6,
            -50000,
        },
        {
            {0, -1, -1, 0, -1, 0},
            {true, false, false, true, false, true},
            6,
            -50000,
        },
        {
            {0, -1, 0, -1, -1, 0},
            {true, false, true, false, false, true},
            6,
            -50000,
        },
        {
            {0, 1, 1, 0},
            {true, false, false, true},
            4,
            100,
        },
        {
            {1, 0, 1, 0},
            {false, true, false, true},
            4,
            100,
        },
        {
            {0, 1, 0, 1},
            {true, false, true, false},
            4,
            100,
        },
        {
            {0, 1, 0},
            {true, false, true},
            3,
            40,
        },
        {
            {0, 1, 1},
            {true, false, false},
            3,
            40,
        },
        {
            {1, 1, 0},
            {false, false, true},
            3,
            40,
        },
        {
            {0, -1, -1, 0},
            {true, false, false, true},
            4,
            -100,
        },
        {
            {-1, 0, -1, 0},
            {false, true, false, true},
            4,
            -100,
        },
        {
            {0, -1, 0, -1},
            {true, false, true, false},
            4,
            -100,
        },
        {
            {0, -1, 0},
            {true, false, true},
            3,
            -100,
        },
        {
            {0, -1, -1},
            {true, false, false},
            3,
            -100,
        },
        {
            {-1, -1, 0},
            {false, false, true},
            3,
            -100,
        },
        {
            {1, 0},
            {false, true},
            2,
            1,
        },
        {
            {0, 1},
            {true, false},
            2,
            1,
        },
        {
            {-1, 0},
            {false, true},
            2,
            1,
        },
        {
            {0, -1},
            {true, false},
            2,
            1,
        },
    },
};
